//
//  ScreenTwoVC.swift
//  delegateTest
//
//  Created by vm mac on 17/11/2016.
//  Copyright © 2016 Michael Aubie. All rights reserved.
//

import UIKit

class ScreenTwoVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPostalCode: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
  
    @IBAction func btnBackPressed(_ sender: AnyObject) {
        
        if(validateEmail(enteredEmail: txtEmail.text!) == true &&
            validatePassword(enteredPassword: txtPassword.text!) == true &&
            validatePostalCode(text: txtPostalCode.text!) == true) {
            dismiss(animated: true, completion: nil)
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtEmail.delegate = self
        txtPostalCode.delegate = self
        txtPassword.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if( textField == txtEmail) {
            if(validateEmail(enteredEmail: textField.text!) == true) {
                return true }
            else { return false }
        }
        else if(textField == txtPassword) {
            if(validatePassword(enteredPassword: textField.text!) == true) {
                return true }
            else { return false }
        }
        else if(textField == txtPostalCode) {
            if(validatePostalCode(text: textField.text!) == true) {
                return true
            } else { return false }
        }
        return true
    }
    
    
    
}
