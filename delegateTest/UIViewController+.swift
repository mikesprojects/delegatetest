//
//  UIViewController+.swift
//  delegateTest
//
//  Created by vm mac on 16/11/2016.
//  Copyright © 2016 Michael Aubie. All rights reserved.
//

import Foundation

import UIKit

extension UIViewController {
    
    func matches(for regex: String, in text: String) -> [String] {
        
        do
        {
            let regex = try NSRegularExpression(pattern: regex)
            let nsString = text as NSString
            let results = regex.matches(in: text, range: NSRange(location: 0,
                                                                 length: nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        }
        catch let error
        {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    func validatePostalCode(text: String) -> Bool {
        
        let postalRegEx = "^[a-z]$|^[a-z]\\d$|^[a-z]\\d[a-z]$|^[a-z]\\d[a-z]\\d$|^[a-z]\\d[a-z]\\d[a-z]$|^[a-z]\\d[a-z]\\d[a-z]\\d$"
        
        if(matches(for: postalRegEx, in: text) != []) { return true }
        else{ return false }
    }
    
    
    func validateEmail(enteredEmail:String) -> Bool {
        
        let emailRegEx = "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}$"

        if(matches(for: emailRegEx, in: enteredEmail) != []) {
            return true
        }
        else { return false }
        
    }
    
    func validatePassword(enteredPassword: String) -> Bool {

        let passwordRegEx = "^(?=.*\\d).{8,18}$"
        
        if(matches(for: passwordRegEx, in: enteredPassword) != []) {
            return true
        }
        else { return false }
    }
    
    
    
}
