//
//  ViewController.swift
//  delegateTest
//
//  Created by vm mac on 16/11/2016.
//  Copyright © 2016 Michael Aubie. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPostalCode: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    
    @IBAction func btnNextScreenPushed(_ sender: AnyObject) {
        
        if(validateEmail(enteredEmail: txtEmail.text!) == true &&
            validatePassword(enteredPassword: txtPassword.text!) == true &&
            validatePostalCode(text: txtPostalCode.text!) == true) {
            performSegue(withIdentifier: "ScreenTwoVCsegue", sender: nil)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        txtEmail.delegate = self
        txtPostalCode.delegate = self
        txtPassword.delegate = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //checks in realtime for valid postal code
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = textField.text! + string
        
        if( textField == txtPostalCode) {
            if(validatePostalCode(text: text) == true) {
                return true
            }
            else { return false }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    //checks when the textField tries to lose first responder status
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if( textField == txtEmail) {
            if(validateEmail(enteredEmail: textField.text!) == true) {
                
                return true }
            else { return false }
        }
        if(textField == txtPassword) {
            if(validatePassword(enteredPassword: textField.text!) == true) {
                
                return true }
            else { return false }
        }
        return true
    }
    
    
}

